#include <stdio.h>
int main(void) {
    int R, C, a[100][100], b[100][100], sum[100][100], i, j;
    printf("Enter the number of rows of the matrix : ");
    scanf("%d", &R);
    printf("Enter the number of columns of matrix : ");
    scanf("%d", &C);

    printf("\nEnter the elements of 1st matrix:\n");
    for (i = 0; i < R; ++i)
        for (j = 0; j < C; ++j) {
            printf("Enter the element a%d%d: ", i + 1, j + 1);
            scanf("%d", &a[i][j]);
        }

    printf("Enter the elements of 2nd matrix:\n");
    for (i = 0; i < R; ++i)
        for (j = 0; j < C; ++j) {
            printf("Enter the element a%d%d: ", i + 1, j + 1);
            scanf("%d", &b[i][j]);
        }

    
    for (i = 0; i < R; ++i)
        for (j = 0; j < C; ++j) {
            sum[i][j] = a[i][j] + b[i][j];
        }

    
    printf("\nSum of the two matrices: \n");
    for (i = 0; i < R; ++i)
        for (j = 0; j < C; ++j) {
            printf("%d   ", sum[i][j]);
            if (j == C - 1) {
                printf("\n\n");
            }
        }

    return 0;
}

