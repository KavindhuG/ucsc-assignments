#include <stdio.h>

void reverse();

void main(void)
{
    printf("Please type anything: ");
    reverse();
}

void reverse()
{
    char c;
    scanf("%c", &c);
    if (c != '\n') {
        reverse();
        printf("%c", c);
    }
}