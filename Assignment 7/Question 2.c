#include <stdio.h>
int main(void) {
    char str[1000], ch;
    int count = 0;

    printf("Please enter any string: ");
    fgets(str, sizeof(str), stdin);

    printf("Please enter any character, of which you want to find the frequency: ");
    scanf("%c", &ch);

    for (int i = 0; str[i] != '\0'; ++i) {
        if (ch == str[i])
            ++count;
    }

    printf("Frequency of %c in this string is = %d", ch, count);
    return 0;
}