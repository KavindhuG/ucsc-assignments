#include<stdio.h>

int main(void)
{
  int c, d, p, q, m, n, k, tot = 0;
  int fst[10][10], sec[10][10], mul[10][10];

  printf(" Please enter the number of rows and columns respectively, for the first matrix \n ");
  scanf("%d%d", &m, &n);

  printf(" Insert your matrix elements, elements are entered towards right side, first row to the last row : \n ");
  for (c = 0; c < m; c++)
    for (d = 0; d < n; d++)
      scanf("%d", &fst[c][d]);
 
  printf(" Please enter the number of rows and columns respectively, for the second matrix\n");
  scanf(" %d %d", &p, &q);

  if (n != p)
    printf(" To be able to multiply matrices, the number of columns of the 1st matrix should be identical to the number of rows of the 2nd matrix, Therefore these two matrices cannot be multiplied. \n ");
  else
  {
    printf(" Insert your matrix elements, elements are entered towards right side, first row to the last row \n ");
 
    for (c = 0; c < p; c++)
      for (d = 0; d < q; d++)
        scanf("%d", &sec[c][d] );

    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          tot = tot + fst[c][k] * sec[k][d];
        }
        mul[c][d] = tot;
        tot = 0;
      }
    }
 
    printf(" The product of the given two matrices is: \n "); 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++)
        printf("%d \t", mul[c][d] );
      printf(" \n ");
    }
  }
  return 0;
}