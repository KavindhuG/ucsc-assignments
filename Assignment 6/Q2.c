#include<stdio.h>
int f(int);

int main(void)
{
  int a, i = 0, b;

  printf("Enter any number:");
  scanf("%d", &a);

  printf("Fibonacci series terms are:\n");

  for (b = 1; b <= a; b++)
  {
    printf("%d\n", f(i));
    i++;
  }

  return 0;
}

int f(int a)
{
  if (a == 0 || a == 1)
    return a;
  else
    return (f(a-1) + f(a-2));
}
